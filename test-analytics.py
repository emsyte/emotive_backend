import concurrent.futures
import subprocess
from concurrent.futures import ThreadPoolExecutor

import requests


def create_observation():
    response = requests.post(
        "https://kioyre7wn5.execute-api.us-east-1.amazonaws.com/production/observation",
        json={"algorithms": ["attention", "emotion"]},
        headers={
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxfQ.n9wka5zyQa6TrBMksFJmalU5X3o7wRBKXT1uTrtAd2Q"
        },
    )
    data = response.json()
    return data["key"]


def finalize(key):
    response = requests.post(
        f"https://kioyre7wn5.execute-api.us-east-1.amazonaws.com/production/observation/{key}/finalize",
        json={"callback": "https://h2lf97gi80.execute-api.us-east-1.amazonaws.com/dev"},
        headers={
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoxfQ.n9wka5zyQa6TrBMksFJmalU5X3o7wRBKXT1uTrtAd2Q"
        },
    )
    data = response.json()
    return key, data


def setup_observation():
    key = create_observation()
    subprocess.run(
        f"aws s3 sync s3://emsyte-analytics-test-data/benchmark s3://emsyte-media-production/emsyte-frames/{key}".split()
    )
    return key


with ThreadPoolExecutor(max_workers=25) as executor:
    futures = [executor.submit(setup_observation) for _ in range(100)]

    keys = []

    for future in concurrent.futures.as_completed(futures):
        keys.append(future.result())

    print(keys)

    futures = [executor.submit(finalize, key) for key in keys]
    results = []

    for future in concurrent.futures.as_completed(futures):
        results.append(future.result())

    print(results)
