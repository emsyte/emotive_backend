from pynamodb.attributes import NumberAttribute
from .utils import FrameDataModel


class EmotionFrame(FrameDataModel, discriminator="EmotionFrame"):
    sort_key_prefix = "EMOTION_FRAME_"

    neutral = NumberAttribute(default=0)
    happiness = NumberAttribute(default=0)
    surprise = NumberAttribute(default=0)
    sadness = NumberAttribute(default=0)
    anger = NumberAttribute(default=0)
