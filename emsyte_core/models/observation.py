import uuid

from pynamodb.attributes import (
    BooleanAttribute,
    JSONAttribute,
    NumberAttribute,
    UnicodeAttribute,
    UnicodeSetAttribute,
    UTCDateTimeAttribute,
)
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection

from .utils import PARTITION_KEY, SORT_KEY, BaseModel

ALGORITHMS = ["attention", "emotion"]


def get_uuid():
    return str(uuid.uuid4())


class ByUserIndex(GlobalSecondaryIndex):
    """
    By User index to lookup all observations for a particular user
    """

    class Meta:
        index_name = "by-user"
        read_capacity_units = 1
        write_capacity_units = 1
        # All attributes are projected
        projection = AllProjection()

    user = UnicodeAttribute(hash_key=True)


class Observation(BaseModel, discriminator="Observation"):
    # This is the key that's returned in the API
    key = UnicodeAttribute(hash_key=True, attr_name=PARTITION_KEY, default=get_uuid)

    # This should always be "metadata"
    _sort = UnicodeAttribute(range_key=True, attr_name=SORT_KEY, default="metadata")

    # User app that this observation belongs to
    user = UnicodeAttribute()

    # URL to send response to when the analysis is done
    callback = UnicodeAttribute(default="", null=True)

    # Optional meta-data that can be sent along with the report
    meta = JSONAttribute(default=dict)

    # The algorithms to run
    algorithms = UnicodeSetAttribute(default=set())

    # Set to true once the consumer has called the /finalize API to signal that all the available frames have
    # been uploaded. The report won't be sent back until the observation has been finalized.
    # Once on observation is finalized, no more segments can be uploaded.
    finalized = BooleanAttribute(default=False)

    # The first step of the calculation is to determine the total number of frames. That total is stored in this field
    total_frames = NumberAttribute(default=0)

    # The final report that's sent to the callback
    report = JSONAttribute(default=dict)

    # When the system started processing the report
    processing_started = UTCDateTimeAttribute(null=True)

    # When the system finished processing the report
    processing_finished = UTCDateTimeAttribute(null=True)

    by_user_index = ByUserIndex()
