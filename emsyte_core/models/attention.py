from pynamodb.attributes import NumberAttribute
from .utils import FrameDataModel


class AttentionFrame(FrameDataModel, discriminator="AttentionFrame"):
    sort_key_prefix = "ATTENTION_FRAME_"
    score = NumberAttribute(default=0)
