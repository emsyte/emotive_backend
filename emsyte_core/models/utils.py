from datetime import datetime, timedelta

from pynamodb.attributes import (
    BooleanAttribute,
    NumberAttribute,
    UnicodeAttribute,
    DiscriminatorAttribute,
)
from pynamodb.constants import META_CLASS_NAME
from pynamodb.models import MetaModel, Model

from emsyte_core import settings

PARTITION_KEY = "pk"
SORT_KEY = "sk"
TIME_TO_LIVE = "ttl"


class DefaultMeta:
    table_name = settings.DYNAMODB_TABLE_NAME
    host = settings.DYNAMODB_HOST


class BaseModelMeta(MetaModel):
    def __init__(cls, name, bases, attrs, **kwargs):
        if META_CLASS_NAME not in attrs:
            attrs[META_CLASS_NAME] = DefaultMeta
            setattr(cls, META_CLASS_NAME, DefaultMeta)
        super().__init__(name, bases, attrs, **kwargs)


class BaseModel(Model, metaclass=BaseModelMeta):
    # Set the TTL for 3 days in the future
    ttl = NumberAttribute(
        attr_name=TIME_TO_LIVE,
        default=lambda: int((datetime.now() + timedelta(days=3)).timestamp()),
    )

    cls = DiscriminatorAttribute()


class FrameDataModel(BaseModel):
    sort_key_prefix: str

    observation_key = UnicodeAttribute(hash_key=True, attr_name=PARTITION_KEY)
    _index_key = UnicodeAttribute(range_key=True, attr_name=SORT_KEY)

    error = BooleanAttribute(default=False)

    @property
    def frame_index(self):
        parts = self._index_key.split(self.sort_key_prefix)
        return int(parts[1])

    @frame_index.setter
    def frame_index(self, frame_index):
        suffix = "%05d" % frame_index
        self._index_key = self.sort_key_prefix + suffix

    @classmethod
    def query_all(cls, key: str):
        return cls.query(
            key, cls._index_key.startswith(cls.sort_key_prefix), scan_index_forward=True
        )

    @classmethod
    def count_all(cls, key: str):
        return cls.count(key, cls._index_key.startswith(cls.sort_key_prefix))
