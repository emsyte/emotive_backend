import logging

from environs import Env

env = Env()

APP_ENV = env("APP_ENV", default="development")

LOG_LEVEL = env.log_level(
    "LOG_LEVEL", default=logging.DEBUG if APP_ENV == "development" else logging.INFO
)

USE_AWS = APP_ENV != "development"

SEGMENT_S3_PRIVATE_BUCKET_NAME = env("SEGMENT_S3_PRIVATE_BUCKET_NAME", default="")
ATTENTION_WINDOW_SIZE = env.int("ATTENTION_WINDOW_SIZE", default=60)
EMOTION_WINDOW_SIZE = env.int("EMOTION_WINDOW_SIZE", default=5)
OBSERVATION_FPS = env.int("OBSERVATION_FPS", default=1)

MEDIA_ROOT = env.path("MEDIA_ROOT", default="./media")

DYNAMODB_HOST = "http://localhost:5001" if APP_ENV == "development" else None

DYNAMODB_TABLE_NAME = env("DYNAMODB_TABLE_NAME", default=f"emsyte-analytics-{APP_ENV}")

SECRET_KEY = env("SECRET_KEY", default="not-a-secret")
FUNCTION_PREFIX = env("FUNCTION_PREFIX", default="")

# SQS Queues
ATTENTION_QUEUE_URL = env(
    "ATTENTION_QUEUE_URL", default="http://0.0.0.0:9324/queue/AttentionQueue"
)
EMOTION_QUEUE_URL = env(
    "EMOTION_QUEUE_URL", default="http://0.0.0.0:9324/queue/EmotionQueue"
)
REPORT_QUEUE_URL = env(
    "REPORT_QUEUE_URL", default="http://0.0.0.0:9324/queue/ReportQueue"
)
OBSERVATION_QUEUE_URL = env(
    "OBSERVATION_QUEUE_URL", default="http://0.0.0.0:9324/queue/ObservationQueue"
)

# If true, won't trigger lambda tasks
SYNCHRONOUS_TASK_RUNNER = False


logging.getLogger("emsyte_core").setLevel(LOG_LEVEL)
