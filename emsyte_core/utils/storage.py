import mimetypes
import os
import tempfile
import uuid
from pathlib import Path, PurePath

import boto3
from botocore.client import ClientError

from emsyte_core import settings


def get_bytes(s: str) -> bytes:
    if isinstance(s, str):
        return s.encode("utf-8")
    return s


class BaseStorage:
    def get_available_name(self, name):
        if self.exists(name):
            return name + uuid.uuid4().hex
        return name

    def open(self, name, mode="rb"):
        """Retrieve the specified file from storage."""
        return self._open(name, mode)

    def save(self, name, content):
        """
        Save new content to the file specified by name. The content should be
        a proper File object or any Python file-like object, ready to be read
        from the beginning.
        """
        # Get the proper name for the file, as it will actually be saved.
        name = self.get_available_name(name)
        return self._save(name, content)

    def delete(self, name):
        """
        Delete the specified file from the storage system.
        """
        raise NotImplementedError(
            "subclasses of Storage must provide a delete() method"
        )

    def exists(self, name):
        """
        Return True if a file referenced by the given name already exists in the
        storage system, or False if the name is available for a new file.
        """
        raise NotImplementedError(
            "subclasses of Storage must provide an exists() method"
        )

    def listdir(self, path):
        """
        List the contents of the specified path. Return a 2-tuple of lists:
        the first item being directories, the second item being files.
        """
        raise NotImplementedError(
            "subclasses of Storage must provide a listdir() method"
        )

    def _save(self, name, content):
        raise NotImplementedError("subclasses of Storage must provide a _save() method")

    def _open(self, name, mode="rb"):
        raise NotImplementedError("subclasses of Storage must provide a _open() method")


class S3Storage(BaseStorage):
    default_content_type = "application/octet-stream"

    def __init__(self, *, location=None, bucket_name=None):
        self.location = location
        self.bucket_name = bucket_name

    def _get_write_parameters(self, name, content=None):
        params = {}

        _type, encoding = mimetypes.guess_type(name)
        content_type = getattr(content, "content_type", None)
        content_type = content_type or _type or self.default_content_type

        params["ContentType"] = content_type

        if encoding:
            params["ContentEncoding"] = encoding

        return params

    def _open(self, name, mode):
        key = self.get_external_name(name)

        if "r" not in mode:
            raise ValueError("S3 files must be opened in read mode")

        try:
            f = tempfile.NamedTemporaryFile(suffix=".S3Storage")
            obj = self.bucket.Object(key)
            obj.download_fileobj(f)
            f.seek(0)
        except ClientError as err:
            if err.response["ResponseMetadata"]["HTTPStatusCode"] == 404:
                raise FileNotFoundError("File does not exist: %s" % name)
            raise  # Let it bubble up if it was some other error
        return f

    def _save(self, name, content):
        key = self.get_external_name(name)
        params = self._get_write_parameters(key, content)
        obj = self.bucket.Object(key)
        obj.upload_fileobj(content, ExtraArgs=params)
        return name

    _bucket = None

    @property
    def bucket(self):
        if self._bucket is None:
            s3 = boto3.resource("s3")
            self._bucket = s3.Bucket(self.bucket_name)
        return self._bucket

    def exists(self, name):
        name = self.get_external_name(name)

        try:
            self.bucket.meta.client.head_object(Bucket=self.bucket.name, Key=name)
            return True
        except ClientError:
            return False

    def delete(self, name):
        name = self.get_external_name(name)
        self.bucket.Object(name).delete()

    def get_put_url(self, name, parameters=None, expire=60 * 30):
        """
        Generate a pre-signed PUT URL to upload a file with the given name
        """

        name = self.get_external_name(name)

        params = parameters.copy() if parameters else {}
        params["Bucket"] = self.bucket.name
        params["Key"] = name

        url = self.bucket.meta.client.generate_presigned_url(
            "put_object", Params=params, ExpiresIn=expire
        )
        return url

    def get_filename(self, key):
        return str(PurePath(key).relative_to(self.location))

    def get_metadata(self, name):
        name = self.get_external_name(name)
        response = self.bucket.meta.client.head_object(
            Bucket=self.bucket.name, Key=name
        )
        return response["Metadata"]

    def get_external_name(self, name):
        path = PurePath(self.location) / name
        return str(path)

    def listdir(self, name):
        path = self.get_external_name(name)
        # The path needs to end with a slash, but if the root is empty, leave
        # it.
        if path and not path.endswith("/"):
            path += "/"

        directories = []
        files = []
        paginator = self.bucket.meta.client.get_paginator("list_objects")
        pages = paginator.paginate(Bucket=self.bucket.name, Delimiter="/", Prefix=path)
        for page in pages:
            for entry in page.get("CommonPrefixes", ()):

                directories.append(
                    PurePath(entry["Prefix"]).relative_to(path).as_posix()
                )
            for entry in page.get("Contents", ()):
                files.append(PurePath(entry["Key"]).relative_to(path).as_posix())
        return directories, files


class LocalFileSystemStorage(BaseStorage):
    def __init__(self, *, location=None):
        self.location = location

    @property
    def base_location(self):
        path = Path(settings.MEDIA_ROOT) / self.location
        return str(path)

    def _open(self, name, mode):
        return open(self.get_external_name(name), mode)

    def _save(self, name, content):
        key = self.get_external_name(name)
        os.makedirs(os.path.dirname(key), exist_ok=True)

        with open(key, "wb") as f:
            if hasattr(content, "read"):
                f.write(get_bytes(content.read()))
            else:
                f.write(content)
        return name

    def delete(self, name):
        os.unlink(self.get_external_name(name))

    def get_filename(self, key):
        return key

    def get_metadata(self, name):
        return {}

    def get_external_name(self, name):
        path = Path(self.base_location) / name
        return str(path)

    def exists(self, name):
        path = self.get_external_name(name)
        return os.path.exists(path)

    def listdir(self, name):
        path = self.get_external_name(name)
        directories, files = [], []
        for entry in os.scandir(path):
            if entry.is_dir():
                directories.append(entry.name)
            else:
                files.append(entry.name)
        return directories, files
