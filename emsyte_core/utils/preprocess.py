import typing
from emsyte_core.models.utils import FrameDataModel


def fill_gaps(
    frames: typing.Sequence[FrameDataModel], data_field: str
) -> typing.List[float]:
    """
    Fill gaps in a series of frames
    """

    index = 0
    last_value = 0.0

    data = []

    for frame in frames:
        # Continue last known value over gaps
        while frame.frame_index > index:
            data.append(last_value)
            index += 1

        last_value = last_value if frame.error else getattr(frame, data_field)
        data.append(last_value)

        index += 1
    return data
