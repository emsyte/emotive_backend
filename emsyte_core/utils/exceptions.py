class DoNotRetryException(Exception):
    """
    There's been an error, but Lambda shouldn't retry the event.
    """


class DuplicateEventException(DoNotRetryException):
    """
    The event has already been processed, so ignore
    """
