from django.apps import AppConfig


class EmsyteConfig(AppConfig):
    name = "core"
