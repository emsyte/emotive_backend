import logging
import os

from emsyte_core.services import preprocess as preprocess_svc
from emsyte_core.services import observation as observation_svc
from emsyte_core.models import Observation, AttentionFrame, EmotionFrame
from emsyte_core.storage import observation_segment_storage, observation_frame_storage

from emsyte_core.tasks.algorithms.attention import process_attention_frame
from emsyte_core.tasks.algorithms.emotion import process_emotion_frame

logger = logging.getLogger(__name__)


def segment_uploaded(event, context):
    """Triggered by an S3 event. Adds the file as a session file, then kicks off the processing"""
    bucket = event["Records"][0]["s3"]["bucket"]["name"]
    key = event["Records"][0]["s3"]["object"]["key"]

    logger.info("Reading segment file with key %s in bucket %s", key, bucket)

    filename = observation_segment_storage.get_filename(key)
    metadata = observation_segment_storage.get_metadata(filename)

    observation_key = metadata.get("observation-key")
    start = float(metadata.get("start"))

    observation = Observation.get(observation_key, "metadata")

    if not observation.finalized:
        preprocess_svc.extract_frames(filename, observation, start)

    observation_segment_storage.delete(filename)

    return {}


def finalize_report(event, context):
    observations = set()

    for record in event["Records"]:
        if record["eventName"] != "INSERT":
            continue

        pk = record["dynamodb"]["Keys"]["pk"]["S"]
        sk = record["dynamodb"]["Keys"]["sk"]["S"]

        if sk.startswith(AttentionFrame.sort_key_prefix) or sk.startswith(
            EmotionFrame.sort_key_prefix
        ):
            observations.add(pk)

    for observation_key in observations:
        observation = Observation.get(observation_key, "metadata")
        observation_svc.check_observation(observation)

    return {}


def add_frame_to_queue(event, context):
    bucket = event["Records"][0]["s3"]["bucket"]["name"]
    key = event["Records"][0]["s3"]["object"]["key"]

    logger.info("Reading frame file with key %s in bucket %s", key, bucket)

    # Filepath should look like <observation-key>/frame_XXXX.jpg
    filepath = observation_frame_storage.get_filename(key)

    observation_key, filename = os.path.split(filepath)

    observation = Observation.get(observation_key, "metadata")

    frame_index = observation_svc.get_frame_index(filename)

    # Process frames
    if "attention" in observation.algorithms:
        process_attention_frame.run(observation_key, frame_index, key)
    if "emotion" in observation.algorithms:
        process_emotion_frame.run(observation_key, frame_index, key)
