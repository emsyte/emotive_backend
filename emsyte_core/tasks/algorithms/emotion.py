import json
import logging

import boto3
from emsyte_core import settings
from emsyte_core.models import Observation
from emsyte_core.services import emotion as emotion_svc
from emsyte_core.services import observation as observation_svc
from emsyte_core.storage import observation_frame_storage
from emsyte_core.utils.exceptions import DoNotRetryException
from emsyte_core.tasks.utils import handler

logger = logging.getLogger(__name__)


EMOTION_FUNCTION_ARN = "arn:aws:lambda:us-east-1:739004611138:function:emotion-recognition-production-emotion-recognition"  # noqa


class InvocationError(Exception):
    pass


def invoke_emotion_aws(frame_key: str):
    payload = {"image": frame_key}

    client = boto3.client("lambda")

    invocation = client.invoke(
        FunctionName=EMOTION_FUNCTION_ARN,
        InvocationType="RequestResponse",
        Payload=json.dumps(payload),
    )

    response = invocation["Payload"].read().decode("utf-8")

    body = json.loads(response)

    if not body.get("success", False):
        raise InvocationError("Unable to analyze frame %s" % frame_key)

    return body["data"]["emotion"]


ZEROS = {"neutral": 0, "happiness": 0, "surprise": 0, "sadness": 0, "anger": 0}


def invoke_emotion_local(frame_key):
    return ZEROS


if settings.USE_AWS:
    invoke_emotion = invoke_emotion_aws
else:
    invoke_emotion = invoke_emotion_local


@handler(settings.EMOTION_QUEUE_URL)
def process_emotion_frame(observation_key: str, frame_index: int, frame_key: str):
    observation = Observation.get(observation_key, "metadata")

    try:
        emotion = invoke_emotion(frame_key)

        emotion_svc.create_emotion_frame(observation, frame_index, emotion, error=False)

    # TODO: Let the error happen, and read the error frame from the dead letter queue
    except InvocationError as e:
        logger.error(
            "Failed to process frame %s on %s: %s", frame_index, observation_key, str(e)
        )
        emotion_svc.create_emotion_frame(observation, frame_index, ZEROS, error=True)
    except DoNotRetryException as e:
        logger.info("Ignoring exception %s", e)
    logger.debug("Processed frame %s on %s", frame_index, observation_key)


def run_emotion(observation: Observation):
    frames = observation_svc.get_frame_set(observation)

    for frame_index, frame_name in frames:
        frame_key = observation_frame_storage.get_external_name(frame_name)
        process_emotion_frame.run(observation.key, frame_index, frame_key)
