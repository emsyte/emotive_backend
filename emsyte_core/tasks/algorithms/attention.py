import json
import logging
import boto3
from emsyte_core import settings
from emsyte_core.models import Observation
from emsyte_core.services import attention as attention_svc
from emsyte_core.services import observation as observation_svc
from emsyte_core.storage import observation_frame_storage
from emsyte_core.utils.exceptions import DoNotRetryException
from emsyte_core.tasks.utils import handler

logger = logging.getLogger(__name__)


class InvocationError(Exception):
    pass


def invoke_attention_aws(frame_key: str):
    payload = {"image": frame_key}

    client = boto3.client("lambda")

    invocation = client.invoke(
        FunctionName="arn:aws:lambda:us-east-1:739004611138:function:gaze-tracking-production-gaze_tracking",
        InvocationType="RequestResponse",
        Payload=json.dumps(payload),
    )
    response = invocation["Payload"].read().decode("utf-8")

    body = json.loads(response)

    if not body.get("success", False):
        raise InvocationError("Unable to analyze frame %s" % frame_key)

    return body["data"]["attention_score"]


def invoke_attention_local(frame_key):
    return 0


if settings.USE_AWS:
    invoke_attention = invoke_attention_aws
else:
    invoke_attention = invoke_attention_local


@handler(settings.ATTENTION_QUEUE_URL)
def process_attention_frame(observation_key: str, frame_index: int, frame_key: str):
    observation = Observation.get(observation_key, "metadata")

    try:
        score = invoke_attention(frame_key)

        attention_svc.create_attention_frame(
            observation, frame_index, score, error=False
        )
    except InvocationError as e:
        logger.error(
            "Failed to process frame %s on %s: %s", frame_index, observation_key, str(e)
        )
        attention_svc.create_attention_frame(observation, frame_index, error=True)
    except DoNotRetryException as e:
        logger.info("Ignoring exception %s", e)
    logger.debug("Processed frame %s on %s", frame_index, observation_key)


def run_attention(observation: Observation):
    frames = observation_svc.get_frame_set(observation)

    for frame_index, frame_name in frames:
        frame_key = observation_frame_storage.get_external_name(frame_name)
        process_attention_frame.run(observation.key, frame_index, frame_key)
