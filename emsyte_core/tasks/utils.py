import json
import logging
import typing

import boto3

from emsyte_core import settings

logger = logging.getLogger(__name__)

sqs = boto3.client(
    "sqs", endpoint_url="http://0.0.0.0:9324" if not settings.USE_AWS else None
)


class TaskHandler:
    def __init__(self, task: typing.Callable, queue_url: str):
        self.task = task
        self.function_name = settings.FUNCTION_PREFIX + task.__name__
        self.queue_url = queue_url

    # Handler
    def __call__(self, event, context):
        for record in event["Records"]:
            body = json.loads(record["body"])
            self.task(*body["args"], **body["kwargs"])
        return {}

    # Runner
    def run(self, *args, **kwargs):
        if settings.SYNCHRONOUS_TASK_RUNNER:
            self.task(*args, **kwargs)
            return

        payload = {"args": args, "kwargs": kwargs}
        logger.debug("Sending to queue %s", self.queue_url)
        sqs.send_message(QueueUrl=self.queue_url, MessageBody=json.dumps(payload))


def handler(queue_url):
    def decorator(func):
        _handler = TaskHandler(func, queue_url)
        return _handler

    return decorator
