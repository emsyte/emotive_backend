import logging

import requests

from emsyte_core import settings
from emsyte_core.models import Observation
from emsyte_core.tasks.utils import handler

logger = logging.getLogger(__name__)


@handler(settings.REPORT_QUEUE_URL)
def send_report(observation_key: str):
    observation = Observation.get(observation_key, "metadata")

    logger.info(
        "Sending report to %s for observation %s" % (observation.callback, observation)
    )

    response = requests.post(
        observation.callback,
        json={
            "meta": observation.meta,
            "algorithms": list(observation.algorithms),
            "key": str(observation.key),
            "measurements_per_second": settings.OBSERVATION_FPS,
            "processing_started": observation.processing_started.isoformat(),
            "processing_finished": observation.processing_finished.isoformat(),
            "report": observation.report,
        },
    )

    # TODO: This should be handled more gracefully
    response.raise_for_status()
