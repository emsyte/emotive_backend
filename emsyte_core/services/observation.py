import logging
import os
import re
import typing
from datetime import datetime, timezone


from emsyte_core.models import Observation
from emsyte_core.storage import observation_frame_storage
from emsyte_core.tasks.report import send_report

FRAME_INDEX_PATTERN = re.compile(r"frame_([0-9]{5})\.jpg")
logger = logging.getLogger(__name__)


def create(user: int, algorithms: set):
    observation = Observation(user=user, algorithms=algorithms)
    observation.save()
    return observation


def calculate(observation: Observation):
    """Start calculating the report for this observation

    Arguments:
        observation {Observation} -- The observation to calculate
    """
    observation.finalized = True
    observation.processing_started = datetime.now(timezone.utc)
    observation.total_frames = len(get_frame_set(observation))
    observation.save()

    logger.info("Starting observation %s" % observation)

    for algorithm in observation.algorithms:
        run_algorithm(algorithm, observation)


def run_algorithm(algorithm: str, observation: Observation):
    # Avoid circular dependency
    from emsyte_core.tasks.algorithms import run_attention, run_emotion

    if algorithm == "attention":
        run_attention(observation)
    if algorithm == "emotion":
        run_emotion(observation)


def check_observation(observation: Observation):
    """Check if a given observation is finished and should be finalized.

    Arguments:
        observation {Observation} -- The observation to log the report to
    """
    if not observation.finalized:
        return

    for algorithm in observation.algorithms:
        if algorithm not in observation.report:
            check_algorithm(observation, algorithm)


def check_algorithm(observation: Observation, algorithm: str):
    """Check if a given algorithm has finished processing on a report yet


    Arguments:
        observation {Observation} -- The observation to log the report to
        algorithm {str} -- The algorithm that's sending the report
    """
    from emsyte_core.services import attention as attention_svc
    from emsyte_core.services import emotion as emotion_svc

    logger.info("Checking status of %s on %s", algorithm, observation.key)

    if algorithm == "attention":
        attention_svc.check_attention_complete(observation)
    if algorithm == "emotion":
        emotion_svc.check_emotion_complete(observation)


def add_report(observation: Observation, algorithm: str, report):
    """Add a report to the observation

    Once all the algorithms have added to a report it will be sent to the callback URL

    Arguments:
        observation {Observation} -- The observation to log the report to
        algorithm {str} -- The algorithm that's sending the report
        report {any} -- The report data
    """
    # observation.refresh_from_db()
    observation.report[algorithm] = report
    observation.save()
    logger.info("Adding report for %s to %s", algorithm, observation)

    if observation.report.keys() == set(observation.algorithms):
        finalize_report(observation)


def finalize_report(observation: Observation) -> None:
    observation.processing_finished = datetime.now(timezone.utc)
    observation.save()

    logger.info(
        "Finished processing %s in %s seconds",
        observation,
        (
            observation.processing_finished - observation.processing_started
        ).total_seconds(),
    )

    send_report.run(observation.key)


def get_frame_set(observation: Observation) -> typing.Set[typing.Tuple[int, str]]:
    """Get the list of frames that have been collected for this observation

    Arguments:
        observation {Observation} -- The observation to gather the frames of

    Returns:
        set -- Set containing tuples of (frame_index, frame_name)
    """

    _, frames = observation_frame_storage.listdir(observation.key)

    frame_set = set()

    for frame in frames:
        index = get_frame_index(frame)
        if index is not None:
            frame_set.add((index, os.path.join(observation.key, frame)))

    return frame_set


def get_frame_index(frame: str) -> typing.Optional[int]:
    """Parse a frame name to get the index

    Arguments:
        frame {str} -- The frame name (in the format of frame_xxxx.jpg)

    Returns:
        int -- The index of the frame (None if the index can't be determined)
    """
    match = FRAME_INDEX_PATTERN.match(frame)
    if not match:
        return None

    index = match.group(1)
    return int(index)
