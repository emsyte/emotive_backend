import logging
import typing
import numpy as np
from emsyte_core import settings
from emsyte_core.models import AttentionFrame, Observation
from emsyte_core.services import observation as observation_svc
from emsyte_core.utils.preprocess import fill_gaps
from emsyte_core.utils.exceptions import DuplicateEventException

logger = logging.getLogger(__name__)


def create_attention_frame(
    observation: Observation, frame_index: int, score: float = 0, error: bool = False
) -> AttentionFrame:
    frames = get_attention_frames(observation)

    index_exists = False

    # Iterate over all the frames to get a count and find the frame
    for frame in frames:
        if frame.frame_index == frame_index:
            index_exists = True

    if index_exists:
        raise DuplicateEventException(
            "Frame with index %s already tracked on observation %s"
            % (frame_index, observation)
        )

    frame = AttentionFrame(
        observation_key=observation.key, error=error, score=(0 if error else score)
    )
    frame.frame_index = frame_index

    frame.save()

    return frame


def get_attention_frames(observation: Observation):
    return AttentionFrame.query_all(observation.key)


def _get_raw_attention(
    observation: Observation, frames: typing.List[AttentionFrame]
) -> list:
    return fill_gaps(frames, "score")


def get_attention_by_frame(
    observation: Observation, frames: typing.List[AttentionFrame]
) -> list:
    raw_attention = _get_raw_attention(observation, frames)

    # Calculate the running average of the attention to smooth out the result
    attention = np.convolve(
        raw_attention,
        np.ones(settings.ATTENTION_WINDOW_SIZE) / settings.ATTENTION_WINDOW_SIZE,
        mode="same",
    )

    return list(attention)


def check_attention_complete(observation: Observation):
    frames = list(get_attention_frames(observation))

    if len(frames) == observation.total_frames:
        attention = get_attention_by_frame(observation, frames)
        observation_svc.add_report(observation, "attention", attention)
