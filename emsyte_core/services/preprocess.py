import logging
import tempfile
from pathlib import Path

import ffmpeg
from emsyte_core import settings
from emsyte_core.models import Observation
from emsyte_core.storage import observation_frame_storage, observation_segment_storage

logger = logging.getLogger(__name__)


def get_ffmpeg_cmd():
    if settings.USE_AWS:
        return "/opt/ffmpeg"

    return "ffmpeg"


def _get_image_paths(video):
    logger.info("Getting images")

    with tempfile.TemporaryDirectory() as image_dir:
        image_path = Path(image_dir)

        ffmpeg.input(video.name).filter("fps", fps=settings.OBSERVATION_FPS).output(
            str(image_path / "frame_%05d.jpg"), format="image2", vcodec="mjpeg"
        ).run(overwrite_output=True, cmd=get_ffmpeg_cmd())

        yield from image_path.iterdir()


def get_images(video, observation: Observation, start: float):
    format = str(observation.key) + "/frame_%05d.jpg"
    index = int(settings.OBSERVATION_FPS * start)

    for image in _get_image_paths(video):
        with image.open("rb") as image_file:
            yield format % index, image_file
        index += 1


def extract_frames(filename: str, observation: Observation, start: float):
    """
    Given the key to a video file, extract all the frames and store them
    """
    logger.info("Extracting frames for segment %s", filename)

    with observation_segment_storage.open(filename) as video:
        for name, image_file in get_images(video, observation, start):
            if observation_frame_storage.exists(name):
                observation_frame_storage.delete(name)

            observation_frame_storage.save(name, image_file)
