from emsyte_core.models import Observation
from emsyte_core.storage import observation_segment_storage

from emsyte_core import settings


def get_presigned_url(observation: Observation, start: float) -> str:
    """Generate a presigned PUT URL for the next segment of the observation

    Arguments:
        observation {Observation} -- The observation to get the presigned URL for
        start {float} -- The start-offset (in seconds) of the

    Raises:
        RuntimeError: If USE_AWS isn't True
        RuntimeError: If the observation has been finalized

    Returns:
        str -- The presigned url
    """

    if not settings.USE_AWS:
        raise RuntimeError("Must be using AWS to get presigned URL")

    if observation.finalized:
        raise RuntimeError(
            "Can't upload segments after an observation has been finalized"
        )

    name = observation_segment_storage.get_available_name("webm/video")

    metadata = {
        "observation-key": str(observation.key),
        "start": str(start * settings.OBSERVATION_FPS),
    }

    return observation_segment_storage.get_put_url(
        name, parameters={"Metadata": metadata, "ContentType": "video/webm"}
    )
