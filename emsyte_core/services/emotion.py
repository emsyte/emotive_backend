import logging
import typing

import numpy as np
from emsyte_core import settings
from emsyte_core.models import EmotionFrame, Observation
from emsyte_core.services import observation as observation_svc
from emsyte_core.utils.exceptions import DuplicateEventException
from emsyte_core.utils.preprocess import fill_gaps

logger = logging.getLogger(__name__)

EMOTIONS = ["neutral", "happiness", "surprise", "sadness", "anger"]


def create_emotion_frame(
    observation: Observation, frame_index: int, emotion: dict, error: bool = False
) -> EmotionFrame:
    frames = get_emotion_frames(observation)

    index_exists = False

    # Iterate over all the frames to get a count and find the frame
    for frame in frames:
        if frame.frame_index == frame_index:
            index_exists = True

    if index_exists:
        raise DuplicateEventException(
            "Frame with index %s already tracked on observation %s"
            % (frame_index, observation)
        )

    frame = EmotionFrame(
        observation_key=observation.key,
        error=error,
        # Data points
        neutral=emotion["neutral"],
        happiness=emotion["happiness"],
        surprise=emotion["surprise"],
        sadness=emotion["sadness"],
        anger=emotion["anger"],
    )

    frame.frame_index = frame_index
    frame.save()

    return frame


def get_emotion_frames(observation: Observation):
    return EmotionFrame.query_all(observation.key)


def _process_emotion(
    frames: typing.List[EmotionFrame], emotion: str
) -> typing.List[float]:
    raw_scores = fill_gaps(frames, emotion)

    processed_scores = np.convolve(
        raw_scores,
        np.ones(settings.EMOTION_WINDOW_SIZE) / settings.EMOTION_WINDOW_SIZE,
        mode="same",
    )

    return list(processed_scores)


def get_emotion_by_frame(
    observation: Observation, frames
) -> typing.Dict[str, typing.List[float]]:
    return dict((emotion, _process_emotion(frames, emotion)) for emotion in EMOTIONS)


def check_emotion_complete(observation: Observation):
    frames = list(get_emotion_frames(observation))

    if len(frames) == observation.total_frames:
        emotion = get_emotion_by_frame(observation, frames)
        observation_svc.add_report(observation, "emotion", emotion)
