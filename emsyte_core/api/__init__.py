import asyncio
import typing
from datetime import datetime, timezone
from functools import partial, wraps

import jsonschema
import jwt
from flask import Flask, request

import emsyte_core.services.observation as observation_svc
import emsyte_core.services.segment as segment_svc
from emsyte_core import settings
from emsyte_core.models.attention import AttentionFrame
from emsyte_core.models.emotion import EmotionFrame
from emsyte_core.models.observation import ALGORITHMS, Observation
import concurrent.futures

from .permissions import get_or_deny_observation

app = Flask(__name__)


def auth(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            token = request.headers["authorization"].lstrip("Bearer").strip()
            decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
            request.user = decoded["user"]
        except Exception as e:
            print(e)
            return {"message": "Unauthorized"}, 401

        return func(*args, **kwargs)

    return wrapper


def validate(schema):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                jsonschema.validate(request.json, schema)
            except jsonschema.ValidationError as e:
                return {"message": e.message}, 400

            return func(*args, **kwargs)

        return wrapper

    return decorator


@app.route("/observation", methods=["POST"])
@auth
@validate(
    {
        "type": "object",
        "properties": {
            "algorithms": {
                "type": "array",
                "items": {"type": "string", "enum": ALGORITHMS},
            }
        },
        "required": ["algorithms"],
    }
)
def start():
    """
    Create an observation
    """
    request.headers["authorization"].lstrip("Bearer ")
    instance = observation_svc.create(
        request.user, algorithms=set(request.json.get("algorithms"))
    )

    return {"key": instance.key}


@app.route("/observation/<observation_key>/get-upload-url", methods=["GET"])
@auth
def get_upload_url(observation_key):
    """
    Get the upload URL for a segment
    """
    observation = get_or_deny_observation(request, observation_key)

    if observation.finalized:
        return {"message": "Observation has already been finalized"}, 409

    start = request.args.get("start", "")
    if not start.isnumeric():
        return {"message": "Start must be an integer"}, 400

    start = int(start)

    return {"upload_to": segment_svc.get_presigned_url(observation, start)}, 200


@app.route("/observation/<observation_key>/finalize", methods=["POST"])
@auth
@validate(
    {
        "type": "object",
        "properties": {"meta": {"type": "object"}, "callback": {"type": "string"}},
        "required": ["callback"],
    }
)
def finalize(observation_key):
    """
    Finalize an observation

    Once an observation is finalized, it won't receive any more segments
    """
    observation = get_or_deny_observation(request, observation_key)

    # Only update observation if it hasn't been finalized yet. This is important because
    # the algorithms or callback shouldn't be allowed to change
    if not observation.finalized:
        observation.meta = request.json.get("meta", {})
        observation.callback = request.json.get("callback")

        observation.processing_started = datetime.now(timezone.utc)
        observation.total_frames = len(observation_svc.get_frame_set(observation))
        observation.finalized = True
        observation.save()

    # It's possible that the observation has already finished processing
    observation_svc.check_observation(observation)

    return {}, 200


async def get_emotion_frame_count(observation_key: str):
    return await asyncio.get_event_loop().run_in_executor(
        None, partial(EmotionFrame.count_all, observation_key)
    )


async def get_attention_frame_count(observation_key: str):
    return await asyncio.get_event_loop().run_in_executor(
        None, partial(AttentionFrame.count_all, observation_key)
    )


async def get_observation_stats(observation: Observation):
    emotion_frame_count, attention_frame_count = await asyncio.gather(
        get_emotion_frame_count(observation.key),
        get_attention_frame_count(observation.key),
    )

    return {
        "observation_key": observation.key,
        "processing_started": observation.processing_started,
        "processing_finished": observation.processing_finished,
        "total_frames": observation.total_frames,
        "algorithms": list(observation.algorithms),
        "emotion_frame_count": emotion_frame_count,
        "attention_frame_count": attention_frame_count,
    }


async def get_all_stats(observations: typing.Iterator[Observation]):
    with concurrent.futures.ThreadPoolExecutor() as pool:
        asyncio.get_event_loop().set_default_executor(pool)

        return await asyncio.gather(
            *(get_observation_stats(observation) for observation in observations)
        )


@app.route("/status", methods=["GET"])
@auth
def get_status():
    """
    Get the status of all the observations running
    """
    # TODO: This API could be made way more efficient by adding a stream processor
    # to aggregate the done frame counts and store on the metadata

    observations = Observation.by_user_index.query(request.user)

    results = asyncio.run(get_all_stats(observations))

    return ({"observations": results}, 200)


@app.route("/observation/<observation_key>/status", methods=["GET"])
@auth
def get_observation_status(observation_key: str):
    """
    Get the status of the given observation
    """
    observation = get_or_deny_observation(request, observation_key)

    status = asyncio.run(get_observation_stats(observation))

    return ({"status": status}, 200)


@app.errorhandler(404)
def not_found(error):
    return {"message": "Not Found"}, 404


@app.errorhandler(500)
def internal_error(error):
    return {"message": "Internal server error"}, 500
