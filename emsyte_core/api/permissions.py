from flask import abort

from emsyte_core.models import Observation


class PermissionDenied(Exception):
    pass


def get_or_deny_observation(request, observation_key):
    try:
        observation = Observation.get(observation_key, "metadata")

        if observation.user != request.user:
            raise PermissionDenied("Not allowed")
    except:
        abort(404)

    return observation
