import pytest

from emsyte_core.models import Observation
from emsyte_core import settings as _settings


@pytest.fixture
def observation():
    observation = Observation(user="bf62a876-ff4c-489f-aa2d-525de1ea28a0")
    observation.save()
    return observation


@pytest.fixture
def settings():
    return _settings


@pytest.fixture
def s3_trigger_event():
    return {
        "Records": [
            {
                "eventVersion": "2.1",
                "eventSource": "aws:s3",
                "awsRegion": "us-east-2",
                "eventTime": "2020-02-22T18:24:48.587Z",
                "eventName": "ObjectCreated:Post",
                "userIdentity": {"principalId": "..."},
                "requestParameters": {"sourceIPAddress": "0.0.0.0"},
                "responseElements": {
                    "x-amz-request-id": "38CD5E6A45E1FA1F",
                    "x-amz-id-2": "kwRcS+tx78f70O9E1flC3QqKxPqZtsUoi4jXGUmBetyRaRvsWQXyLRnBven60N+IwY9CMs4P5P7vjWGAmF9CClo8feIEPRH3",
                },
                "s3": {
                    "s3SchemaVersion": "1.0",
                    "configurationId": "bf9f5c11-400d-4da8-be4c-d5e156601289",
                    "bucket": {
                        "name": "emsyte-media",
                        "ownerIdentity": {"principalId": "..."},
                        "arn": "arn:aws:s3:::emsyte-media",
                    },
                    "object": {
                        "key": "webm/video",
                        "size": 68335,
                        "eTag": "c15bb0393da12b2a8dfbf74a5b130132",
                        "sequencer": "005E5171EF4DF86DCB",
                    },
                },
            }
        ]
    }
