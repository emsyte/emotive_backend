from datetime import datetime, timezone
from io import StringIO
from unittest.mock import patch

from emsyte_core.models import Observation
from emsyte_core.services import observation as observation_svc
from emsyte_core.storage import observation_frame_storage


def test_get_frame_set(observation, tmpdir, settings):
    settings.MEDIA_ROOT = tmpdir

    for i in range(4):
        observation_frame_storage.save(
            "%s/frame_%05d.jpg" % (observation.key, i), StringIO("")
        )

    frames = observation_svc.get_frame_set(observation)
    assert frames == {
        (0, f"{observation.key}/frame_00000.jpg"),
        (1, f"{observation.key}/frame_00001.jpg"),
        (2, f"{observation.key}/frame_00002.jpg"),
        (3, f"{observation.key}/frame_00003.jpg"),
    }


def test_add_report(observation):
    observation.algorithms = ["attention"]
    observation.save()

    class send_report:
        calls = 0

        @classmethod
        def run(cls, _):
            cls.calls += 1

    observation.processing_started = datetime.now(timezone.utc)

    with patch.object(observation_svc, "send_report", send_report):
        observation_svc.add_report(observation, "attention", {"data": "test"})

    observation = Observation.get(observation.key, "metadata")

    assert observation.report.keys() == {"attention"}
    assert observation.report["attention"] == {"data": "test"}

    assert send_report.calls == 1
