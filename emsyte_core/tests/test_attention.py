from io import StringIO

import pytest

from emsyte_core.storage import observation_frame_storage
from emsyte_core.tasks.algorithms import run_attention
from emsyte_core.models import AttentionFrame
from emsyte_core.services.attention import (
    get_attention_frames,
    get_attention_by_frame,
    _get_raw_attention,
    create_attention_frame,
)


def test_attention(observation, tmpdir, settings):
    settings.MEDIA_ROOT = tmpdir
    settings.SYNCHRONOUS_TASK_RUNNER = True

    observation.total_frames = settings.ATTENTION_WINDOW_SIZE
    observation.save()

    for i in range(settings.ATTENTION_WINDOW_SIZE):
        observation_frame_storage.save(
            "%s/frame_%05d.jpg" % (observation.key, i), StringIO("")
        )

    run_attention(observation)

    frames = list(get_attention_frames(observation))

    assert len(frames) == settings.ATTENTION_WINDOW_SIZE
    assert (
        get_attention_by_frame(observation, frames)
        == [0.0] * settings.ATTENTION_WINDOW_SIZE
    )


def test_attention_frames(observation):
    observation.total_frames = 10
    observation.save()

    for i in [2, 4, 5]:
        frame = AttentionFrame(observation_key=observation.key, score=i, error=False)
        frame.frame_index = i
        frame.save()

    frames = get_attention_frames(observation)
    assert _get_raw_attention(observation, frames) == [0, 0, 2, 2, 4, 5]


def test_attention_frames_report(observation, settings):
    n = 120
    half_window_size = settings.ATTENTION_WINDOW_SIZE / 2

    def get_value_index(i):
        if i < half_window_size:
            return i
        return n - i

    observation.total_frames = n
    observation.save()

    for i in range(n):
        create_attention_frame(observation, i, 1)

    expected = []
    for i in range(n):
        index = get_value_index(i)
        if index < half_window_size:
            expected.append(0.5 + (index / settings.ATTENTION_WINDOW_SIZE))
        else:
            expected.append(1)

    frames = get_attention_frames(observation)
    data = get_attention_by_frame(observation, frames)

    assert data == pytest.approx(expected)
