# def test_create_observation(client, user):
#     client.login(username="test", password="password")
#     response = client.post("/api/v1/observation/")

#     assert response.status_code == 201
#     assert response.json() == {
#         "key": str(Observation.objects.order_by("-created").first().key)
#     }


# def test_calculate_observation(client, observation, settings, tmpdir, requests_mock):
#     settings.MEDIA_ROOT = tmpdir

#     for i in range(4):
#         observation_frame_storage.save(
#             "%s/frame_%04d.jpg" % (observation.key, i), StringIO("")
#         )

#     requests_mock.post("http://example.com")
#     client.login(username="test", password="password")
#     response = client.post(
#         f"/api/v1/observation/{observation.key}/calculate/",
#         data={"algorithms": ["attention"], "callback": "http://example.com"},
#     )

#     assert response.status_code == 200

#     observation = Observation.objects.get(pk=observation.pk)

#     assert observation.algorithms == ["attention"]
#     assert observation.callback == "http://example.com"

#     request = requests_mock.request_history[0]

#     assert request.url == "http://example.com/"
#     assert request.json() == {
#         "algorithms": ["attention"],
#         "meta": {},
#         "measurements_per_second": 1,
#         "report": {"attention": [0, 0, 0, 0, 0]},
#     }


# def get_put_url(name, parameters):
#     return "<url>"


# @patch.object(observation_segment_storage, "get_put_url", get_put_url, create=True)
# def test_get_segment_url(client, observation, settings):
#     settings.USE_AWS = True

#     client.login(username="test", password="password")
#     response = client.get(
#         f"/api/v1/observation/{observation.key}/get-upload-url/?start=0"
#     )
#     assert response.status_code == 200
#     assert response.json() == {"upload_to": "<url>"}
