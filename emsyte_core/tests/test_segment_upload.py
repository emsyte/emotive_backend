import pytest
from emsyte_core.services import segment
from emsyte_core.storage import observation_segment_storage, observation_frame_storage
from unittest.mock import patch

from emsyte_core.tasks.handlers import segment_uploaded


@pytest.mark.parametrize("start", [0, 10, 5])
def test_get_segment_url(start, tmpdir, observation, settings):
    """Test that get_segment_url returns the segment URL"""
    settings.USE_AWS = True
    settings.MEDIA_ROOT = tmpdir

    def get_put_url(name, parameters):
        assert name == "webm/video"
        assert parameters.keys() == {"Metadata", "ContentType"}
        assert parameters["Metadata"] == {
            "observation-key": observation.key,
            "start": str(start),
        }

        return "<url>"

    with patch.object(
        observation_segment_storage, "get_put_url", get_put_url, create=True
    ):
        url = segment.get_presigned_url(observation, start)
        assert url == "<url>"


def test_cant_get_segment_if_observation_finalized(observation, settings):
    """Test that if the observation has been finalized get_segment_url throws an error"""
    settings.USE_AWS = True

    observation.finalized = True
    with pytest.raises(RuntimeError):
        segment.get_presigned_url(observation, 0)


def test_get_segment_requires_aws(observation, settings):
    settings.USE_AWS = False
    with pytest.raises(RuntimeError):
        segment.get_presigned_url(observation, 0)


def test_segment_uploaded_handler(s3_trigger_event, observation, settings, tmpdir):
    """Test that segment uploads are properly converted into frames"""

    settings.USE_AWS = False
    settings.MEDIA_ROOT = tmpdir

    metadata = {"observation-key": observation.key, "start": "0"}

    # Add the segment video to storage
    with open("./emsyte_core/tests/data/segment.webm", "rb") as f:
        observation_segment_storage.save("webm/video", f)

    # Trigger the handler
    with patch.object(observation_segment_storage, "get_metadata", lambda x: metadata):
        segment_uploaded(s3_trigger_event, None)

    assert (
        observation_segment_storage.exists("webm/video") == False
    ), "Source webm file should be deleted after frames are processed"

    dirs, files = observation_frame_storage.listdir(str(observation.key))
    assert dirs == []
    assert set(files) == {"frame_%05d.jpg" % i for i in range(10)}


def test_segment_uploaded_skip_after_observation_starts(
    s3_trigger_event, observation, settings, tmpdir
):
    """After an observations starts, the segment upload handler should just delete the file"""

    settings.MEDIA_ROOT = tmpdir

    metadata = {"observation-key": observation.key, "start": "0"}

    # Add the segment video to storage
    with open("./emsyte_core/tests/data/segment.webm", "rb") as f:
        observation_segment_storage.save("webm/video", f)

    observation.finalized = True
    observation.save()

    # Trigger the handler
    with patch.object(observation_segment_storage, "get_metadata", lambda x: metadata):
        segment_uploaded(s3_trigger_event, None)

    assert (
        observation_segment_storage.exists("webm/video") == False
    ), "Source webm file should be deleted after frames are processed"

    assert not observation_frame_storage.exists(observation.key)


def test_segment_uploaded_handler_multiple_segments(
    s3_trigger_event, observation, settings, tmpdir
):
    """Test that multiple segments can be uploaded and that overlapping segments are handled correctly"""

    settings.MEDIA_ROOT = tmpdir
    settings.USE_AWS = False

    metadatas = [
        {"observation-key": observation.key, "start": "0"},
        {"observation-key": observation.key, "start": "5"},
    ]

    for metadata in metadatas:
        # Add the segment video to storage
        with open("./emsyte_core/tests/data/segment.webm", "rb") as f:
            name = observation_segment_storage.save("webm/video", f)
            key = observation_segment_storage.get_external_name(name)

        s3_trigger_event["Records"][0]["s3"]["object"]["key"] = key

        # Trigger the handler
        with patch.object(
            observation_segment_storage, "get_metadata", lambda x: metadata
        ):
            segment_uploaded(s3_trigger_event, None)

    dirs, files = observation_frame_storage.listdir(observation.key)
    assert dirs == []
    assert set(files) == {"frame_%05d.jpg" % i for i in range(15)}
