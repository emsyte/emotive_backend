from io import StringIO


from emsyte_core.storage import observation_frame_storage
from emsyte_core.tasks.algorithms import run_emotion
from emsyte_core.services.emotion import (
    get_emotion_frames,
    get_emotion_by_frame,
    _process_emotion,
    create_emotion_frame,
)


EMOTION_LABELS = ["neutral", "happiness", "surprise", "sadness", "anger"]


def test_emotion(observation, tmpdir, settings):
    settings.MEDIA_ROOT = tmpdir
    settings.SYNCHRONOUS_TASK_RUNNER = True

    observation.total_frames = settings.EMOTION_WINDOW_SIZE
    observation.save()

    for i in range(settings.EMOTION_WINDOW_SIZE):
        observation_frame_storage.save(
            "%s/frame_%05d.jpg" % (observation.key, i), StringIO("")
        )

    run_emotion(observation)

    assert get_emotion_by_frame(observation, list(get_emotion_frames(observation))) == {
        "neutral": [0.0] * settings.EMOTION_WINDOW_SIZE,
        "happiness": [0.0] * settings.EMOTION_WINDOW_SIZE,
        "surprise": [0.0] * settings.EMOTION_WINDOW_SIZE,
        "sadness": [0.0] * settings.EMOTION_WINDOW_SIZE,
        "anger": [0.0] * settings.EMOTION_WINDOW_SIZE,
    }
