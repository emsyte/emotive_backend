# mypy: ignore-errors
from emsyte_core import settings
from emsyte_core.utils.storage import S3Storage, LocalFileSystemStorage

if settings.USE_AWS:
    observation_segment_storage = S3Storage(
        location="emsyte-segments", bucket_name=settings.SEGMENT_S3_PRIVATE_BUCKET_NAME
    )
    observation_frame_storage = S3Storage(
        location="emsyte-frames", bucket_name=settings.SEGMENT_S3_PRIVATE_BUCKET_NAME
    )
else:
    observation_segment_storage = LocalFileSystemStorage(location="emsyte-segments")
    observation_frame_storage = LocalFileSystemStorage(location="emsyte-frames")
