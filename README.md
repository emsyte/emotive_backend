# Emsyte

Emsyte Analytics API

## Analytics Architecture

https://lucid.app/lucidchart/f0f23397-8316-42af-b99c-307c69a8b4bc/edit?docId=f0f23397-8316-42af-b99c-307c69a8b4bc&shared=true&page=0_0#?folder_id=home&browser=icon

## Getting Started

Before you being, you'll need to install [pipenv](https://pipenv.readthedocs.io/en/latest/) with python 3.8.

Than follow these steps to setup:

1. Clone the repo
2. Install python dependencies:
```bash
  pipenv install && pipenv install --dev
```
3. Install node dependencies:
```bash
npm install
```
3. Install local DynamoDB
```bash
npx sls dynamodb install
```
4. Start local development server
```bash
npx sls offline start
```

## Architecture

The app runs on AWS Lambda and is deployed using [serverless](https://serverless.com). Files are uploaded to S3 buckets, data is persisted in DynamoDB, and all messaging is managed by SQS queues.

This app provides the analytics backend for a service that uploads video data of a person and generates a report of that persons emotional status and attention throughout the video.

For the purpose this readme, ANALYTICS SERVICE will refer to this service, APPLICATION will refer to the application that is integrating with this service, and CLIENT will refer to the browser client that the person is using whose webcam data is used.

The full process from webcam to report is this:

1. The APPLICATION will create an observation (using `/observation` endpoint)
2. The CLIENT will periodically upload video chunks from the webcam feed. It will get the upload API by hitting an API on the APPLICATION which will in tern hit an API on the ANALYTICS SERVICE to get the upload url for the next segment (`/observation/<observation_key>/get-upload-url` API endpoint)
3. After the video is done, the APPLICATION will call the API to start processing the observation (`/observation/<observation_key>/calculate`). It will send a callback URL, which the ANALYTICS SERVICE will post the final report to when it's ready.

## Working with the project

### Running locally
To run the application locally, it needs to spin up a local DyanmoDB instance and an offline SQS interface.

For the local DynamoDb instance, this project uses [serverless-dynamodb-local](https://www.npmjs.com/package/serverless-dynamodb-local)

To install the local DynamoDb instance, you can run:

```bash
npx sls dynamodb install
```

After that, serverless will manage the rest, so you shouldn't need to worry about it.

For SQS, this project uses [serverless-offline-sqs](https://www.npmjs.com/package/serverless-offline-sqs)

You'll need to run the SQS service separately:

```bash
docker run -it -p 9324:9324 s12v/elasticmq:latest
```

After that, running serverless should get the project up and running:

```bash
npx sls offline start
```

### Running for development
To get instant restarting or access to debugging, you need to run the service a bit differently.

Start by running dynamodb:

```bash
sls dynamodb start
```

Then, in a new tab, run the WSGI application:

```bash
sls wsgi serve
```

### Type checks

Running type checks with mypy:

```bash
pipenv run mypy emsyte_core
```

### Test coverage

To run the tests, check your test coverage, and generate an HTML coverage report

```bash
coverage run -m pytest
coverage html
open htmlcov/index.html
```
### Running tests with py test

Before running tests, you first must have an instance of dynamodb running. To do that, you can simply start the local development server:

```bash
npx sls offline start
```

```bash
pipenv run pytest
```
