#!/bin/sh

# See: https://serverless.com/blog/publish-aws-lambda-layers-serverless-framework/#example-use-case-creating-gifs-with-ffmpeg
mkdir ffmpeg-layer
cd ffmpeg-layer
wget https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-amd64-static.tar.xz
tar xf ffmpeg-git-amd64-static.tar.xz ffmpeg-git-*-amd64-static
mv ffmpeg-git-*-amd64-static/ffmpeg .
rm -rf ffmpeg-git-*-amd64-static
rm ffmpeg-git-amd64-static.tar.xz
cd ..
